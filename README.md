# Prout-o-mètre

Librement inspiré du bingo du troll issu du site [Grisebouille](https://grisebouille.net/le-bingo-du-troll/) de Gee
et décliné en version interactive sur <https://troll.framasoft.org> par JosephK

[![](https://grisebouille.net/wp-content/uploads/2016/02/lf_013_le_bingo_du_troll-1024x633.png)](https://troll.framasoft.org)

Jeu d'auto-défense intellectuelle contre celles et ceux qui proutent des pensées nauséabondes afin d'ouvrir [la fenêtre d'Ouverton](https://fr.wikipedia.org/wiki/Fen%C3%AAtre_d'Overton) à leur avantage.